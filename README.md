# dotfiles creed

These are my dotfiles. There are many like them, but these are mine. 

My dotfiles are my best friend. They are my environment. I must master them as I must master my environment.

Without me, my dotfiles are useless. Without my dotfiles, I am useless. I must keep my dotfiles updated. I must keep them more updated than all of the other maintainers who are trying to keep their dotfiles updated. I must update before they update. I will ...

My dotfiles and I know that what counts in dotfiles is not the number of lines, the comments we add, nor the installers we create. We know that it is the updates that count. We will update ...

My dotfiles are human, even as I, because they are my environment. Thus, I will learn them as a brother. I will learn their weaknesses, their strength, their dependencies, their libraries, their configs and their arguments. I will keep my dotfiles updated and commented. We will become part of each other. We will ...

Before /r/dotfiles, I swear this creed. My dotfiles and I are the ricers of this system. We are the masters of our workflows. We are the saviors of my shell.

So be it, until configuration is complete and there are no defaults, but sanity!